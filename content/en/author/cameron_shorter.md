---
title: Cameron Shorter
image: /uploads/blog/authors/cameron_shorter.jpg
description: Accidental technical communicator; Engineer; Community builder.
follow: Read more from Cameron on his [personal blog](http://cameronshorter.blogspot.com/).
web: http://cameronshorter.blogspot.com/
email: cameron.shorter@gmail.com
social:
  github: camerons
  gitlab: camerons1
  linkedin: cameron-shorter
  medium: ""
  twitter: ""
  instagram: ""
type: author
lastmod: 2023-05-15T06:51:09.733Z
---
Cameron became a technical communicator by accident. From engineer, he moved to technical consultant, business analysis, sales support, and then oops, … he discovered he'd become a technical communicator.

He's helped build open source software projects, including co-founding The Good Docs Project.