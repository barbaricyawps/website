---
title: Welcome to The Good Docs Project
linkTitle: Welcome
lastmod: 2022-12-03T02:00:33.466Z
---

{{% blocks/lead color="primary" %}}

# Welcome to The Good Docs Project

We're glad you are interested in joining us.

{{% /blocks/lead %}}

{{% blocks/section color="white" type="section" %}}

To get you started in our community, we'd like to invite you to our next Welcome Wagon meeting.
At this 30-minute orientation meeting, you'll get:

* A brief overview of our project's goals and mission.
* A bit of information about our community and reasons to consider joining.
* An overview of our key initiatives and working groups that you might consider contributing to.
* A chance to tell us your goals for contributing as well as your current skill/experience levels to see if we can find a working group and a good first task for you.

In order to register for this meeting, we need you to fill out the following form.
Your privacy will be respected, and this information will only be shared with people in the project on a need-to-know basis.

<script src="https://www.cognitoforms.com/f/seamless.js" data-key="6tiz94CtckepmzIMR8C54Q" data-form="2"></script>

{{% /blocks/section %}}
