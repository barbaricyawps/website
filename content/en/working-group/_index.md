---
title: Working Groups
description: Find and join our Working Groups to help the Good Docs Project progress toward our shared goals.
draft: false
---

{{% blocks/lead color="primary" %}}

# The Good Docs Project working groups

Learn more about The Good Docs Project working groups and join one today!

{{% /blocks/lead %}}

{{% blocks/section color="white" type="section" %}}

## What are working groups?
A working group consists of two or more project contributors who are collaborating together to work on a Good Docs Project initiative or focus area of the project.


## Who can join?

If you are new to The Good Docs Project, begin by filling out the [Welcome Wagon registration form](https://thegooddocsproject.dev/welcome/) to receive an invitation to the project.

After you've joined the project, all working groups are open for anyone to join as a general rule. Contributors can belong to as many working groups as they have time for and it’s okay to join a group to observe for a while before contributing.
If you are interested in contributing to a working group:

- Talk to the working group lead(s)
- Join the working group Slack channel (channel names are in the group's description page)
- Check the [community calendar](https://thegooddocsproject.dev/community/#calendar) for meeting times and join a meeting


## What is the time commitment?

The various working groups generally meet weekly or bi-weekly for an hour.
Most community members give about 1-2 hours of their time every week by participating in one of these groups.

Always remember that we’ll take what you can give. You and your family come first, then work, then volunteering.
If you can’t keep your working group commitments, that’s okay. Just let your working group leader know.


## Which working groups are available?

Click any of the working groups below to learn more about what that group is working on.

{{% /blocks/section %}}