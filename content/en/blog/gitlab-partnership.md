---
draft: false
title: We Are GitLab Open Source Partners
date: 2023-06-20
publishDate: 2023-06-20
lastmod: 2023-06-20T01:39:20.842Z
image: /uploads/blog/headers/thegooddocs-gitlab-partnership.jpg
images:
  - /uploads/blog/headers/thegooddocs-gitlab-partnership.jpg
author:
  - good_docs
tags:
  - partnership
  - gitlab
  - open-source
custom_copyright: ""
custom_license: ""
type: blog
---

May has been an exciting month as The Good Docs Project has been invited to become a [GitLab Open Source Partner](https://about.gitlab.com/solutions/open-source/partners/). Joining forces with GitLab brings us new opportunities to promote best practices in technical documentation for open source software projects.

It also provides a platform to help us shine a light on the valuable contributions that individuals from non-code backgrounds can bring to the open source community.

So what's the story behind our collaboration? It all started last year when we decided to migrate our project from GitHub to GitLab.

## Why The Good Docs Project migrated to GitLab

Since our [founding in 2019](https://thegooddocsproject.dev/blog/origin-story-birth-of-the-good-docs-project/), we've been avid users of GitHub. However, we noted that while it's free to use for open source projects, GitHub itself is a closed source project.

GitLab, by contrast, fully embraces the open source philosophy of welcoming contributions to its source code from everyone. This aligns more closely with the very principles of community involvement, transparency, and freedom to modify and redistribute, that we all know open source to be.

GitLab provides a project-wide view of our many initiatives and projects, allowing us to dial into the different tasks scheduled for release and functioning as a central source of truth. We also found GitLab’s review workflows for text documents especially easy to work with, which streamlines our template review process.

We document all decisions that have a major impact on our project—read more about why we moved our Git repositories in [RFC 009 Migrate to GitLab](https://gitlab.com/tgdp/request-for-comment/-/blob/main/Accepted-RFCs/RFC-009-migrate-to-gitlab.md).


## About GitLab Open Source Partners

In GitLab's own words, the [program](https://about.gitlab.com/handbook/marketing/community-relations/community-programs/opensource-program/#gitlab-open-source-partners) aims to:

>"...build relationships with prominent open source projects using GitLab as a critical component of their infrastructure. By building these relationships, GitLab hopes to strengthen the open source ecosystem."

By using GitLab infrastructure, partners provide GitLab with valuable feedback and data. Both parties benefit from co-marketing and community collaboration.

We join the ranks of many larger organizations that have opted to become open source partners. As a [keystone project](https://thegooddocsproject.dev/blog/becoming-a-keystone-project/), we play a prominent role in the broader open source landscape, and we’re aiming to demonstrate how good documentation has a disproportionate positive impact in the software ecosystem.


## What GitLab Open Source Partners means for us

Partnering with GitLab is all about strengthening the open source ecosystem. At base, GitLab Open Source Partners provides certain benefits to open source projects and groups that rely on GitLab.

The Good Docs Project sees the relationship as a win-win. Through the partner program, GitLab is building relationships with open source projects and communities, and exploring avenues for feedback and collaboration. 

We hope to be able to share our experience of using the GitLab platform. As a project that is primarily working in prose (rather than code), we are in a position to provide feedback to GitLab about how we use their platform for collaborative, non-code workflows. The program gives us a direct line of communication to GitLab, which should allow us to prioritize functional requests to better support our editorial workflow—outcomes that should benefit any projects working with docs in GitLab.

The partnership raises our visibility in GitLab marketing channels. We’ll gain opportunities to meet with, and learn from, other open source partners. We should be able to connect with organizations who have a keen interest in adopting best practices, and get feedback and beta testing on our [templates](https://gitlab.com/tgdp/templates/-/tree/main). It also helps us spread the good word about our templates. The partner program provides a larger platform on which [The Good Docs Project](https://thegooddocsproject.dev/about/) can share technical documentation best practices to help improve the [productivity](tinyurl.com/docsfactpack) of open source projects.


## Final thoughts

We're looking forward to this exciting opportunity to be part of a larger community and help open source projects work better with our tools, methods, and frameworks.

We believe that teaming up with GitLab is a big step forward for The Good Docs project. It means we're no longer a small group of passionate tech writers, but now have the backing of a major tech foundation like GitLab. This change matches our new focus of building a diverse user community from our base of tech writing professionals.

We can't wait to make good things happen for the open source community.