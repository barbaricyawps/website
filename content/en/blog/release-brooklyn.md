---
draft: false
title: Template release 0.3.0 & using our own templates
date: 2022-12-16T02:25:48.062Z
publishDate: 2022-12-16T02:25:48.062Z
lastmod: 2022-12-18T00:29:07.555Z
image: /uploads/blog/headers/release-brooklyn.jpg
images:
  - /uploads/blog/headers/release-brooklyn.jpg
author:
  - ryan_macklin
tags:
  - the-project
  - releases
  - testing
custom_copyright: ""
custom_license: ""
type: blog
---

The Good Docs Project just published our latest release of documentation templates! This [version 0.3.0](https://gitlab.com/tgdp/templates/-/releases/v0.3.0) (codenamed "Brooklyn," as all of our releases are named after bridges) features four new templates:
* API reference
* Code of conduct
* README
* Release notes

Even more, we did a massive overhaul of our how-to template—which was a pretty huge project in and of itself.

A huge thank-you to all our template contributors! You can read more about the contents of this release and the contributors involved in **[our release notes in GitLab](https://gitlab.com/tgdp/templates/-/releases/v0.3.0)**.

... Which brings us to the substance of this post: the idea of using one's own project or product (often called "dogfooding"). Dogfooding goes beyond the idea of testing one's product and overall good quality assurance practices, to living and breathing what you're making. If you struggle to use your product in a normal, reasonable situation, you're the first line of feedback for making it better!

That sounds obvious, but in many cases we aren't building or supporting products for our own use. I certainly didn't when I wrote for marketing software. I couldn't rely on my own experiences, even having a test account to play with, because I wasn't my own target audience. But when it comes to the passionate work the Good Docs Project contributors do, we're part of our actual audience!

In this case, we dogfooded our release notes template by using it to make the release notes! (Yes, how meta.)

That said, there are a couple points to always have in mind when dogfooding your own projects.

## It's not all about you

Saying "we're part of our actual audience," it's key to emphasize *"part of."* Even then, the direct contributors are too close to the project to provide the same quality of feedback that general users do. Our minds are loaded with additional knowledge and context not available to general users. Thankfully for our project, not everyone in TGDP works on templates, so we have some users who can give feedback directly to the creators.

Still, even the rest of us might know context and material that you reading this blog doesn't have, so when we're taking in feedback, we have to keep in mind *it's not all about us.* We need independent feedback, for people lacking development context to tell us what works, what doesn't, and how they feel overall.

## The "rule of twice"

If two people independently give the same sort of feedback, *we absolutely need to process it.* That's a rule I learned in game design, that keeps designers honest about handling feedback that might otherwise sting. After all, we're humans working on something with passion, so criticism can come emotions. And one emotional response is to shut down or ignore a bit of feedback.

This isn't something necessarily done maliciously or even consciously. It's absolutely human to feel a sting when they care about something, and want to avoid a hard feeling. And that's where the "rule of twice" plays in: it creates a hard structure to help counteract that avoidance. It says "you might want to avoid it, but you know you need to process it."

Processing that advice could be changing something, clarifying something, or even deciding that feedback is a feature rather than a bug. As long as you spend some time considering the feedback and make an honest evaluation regarding it, you're doing well be your users.

You might as "why not 'rule of once?'" That's a fair question, and often we do, but the rule exists for times when it's hard to handle feedback—because it's overwhelming, or life is busy, or there are too many conflicting priorities, and so on.



That's all for this post. If you try our templates, we'd love to get your feedback! [Join our Slack](https://slack.thegooddocsproject.dev/) and share your thoughts on *#blog-comments*.

---

Photo credit: Michał Ludwiczak via [pexels.com](https://www.pexels.com/photo/photography-of-bridge-during-nighttime-1239162/)