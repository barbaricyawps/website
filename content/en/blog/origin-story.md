---
draft: false
title: Origin Story - Birth of The Good Docs Project
date: 2023-06-08
publishDate: 2023-06-08
lastmod: 2023-06-08T05:05:35.657Z
image: /uploads/blog/headers/babydoctopus.png
images:
  - /uploads/blog/headers/babydoctopus.png
author:
  - good_docs
tags:
  - the-project
custom_copyright: ""
custom_license: null
type: blog
---



{{% alert title="About the project" color="warning" %}}
The Good Docs Project is an open source community of technical writers, doc tools experts, UX designers, and software engineers who are committed to improving the quality of documentation in open source. We aim to educate and empower people to create high-quality documentation by providing them with resources, best practices, and tools to enhance their documentation in open source and beyond.
{{% /alert %}}


The Good Docs Project was formed in 2019, and sprung from an idea had by co-founder, [Cameron Shorter](http://cameronshorter.blogspot.com/), when he was involved with Google’s inaugural [Season of Docs](https://opensource.googleblog.com/2019/03/introducing-season-of-docs.html) program.

> _Surprisingly, the world didn’t have an open, best-practices suite of writing templates. A group of documentarians and open-source community members decided to create them, and we banded together and called ourselves The Good Docs Project. We want to collate the collective wisdom of our communities into templates, writing instructions and background theory._—Cameron Shorter

Spearheaded in Australia, but quickly attracting expert tech writers from across the globe, early contributors came from open-source communities such as the [OSGeo Foundation](https://www.osgeo.org/) and [Write the Docs](https://www.writethedocs.org/), plus big tech giants like Google, Uber, and Spotify. Quickly, we found ourselves with a diverse, incredibly talented pool of people caring about documentation.

### Our beginnings

Spinning up an open source project requires a bit of effort, but everyone is keen in the early days. In the first meetings and discussions we tackled the big issues: communication tools (email and Slack!), the project name, and our logo design - the Doctopus.

![Doctopus](/uploads/blog/authors/doctopus-blue.png "A tech writer needs many arms to create great docs") 

When the project was 3 months old, we held a “docs fix-it” session at [Write The Docs Australia conference](https://www.writethedocs.org/conf/australia/2019/), where ~50 documentarians reviewed the templates in our 0.1 alpha release which included:

* API Overview
* API Quickstart
* API Reference
* How-to
* Logging
* Reference
* Tutorial

We continue to have a presence at [Write the Docs](https://www.writethedocs.org) conferences around the world. This community is a great place to seed ideas for The Good Docs Project, to share and test our work, and to find contributors. Over the years we have become a beacon for technical writers, and have enjoyed a constant stream of new contributors flocking to our project.


## Key milestones

In June 2022, we cemented our [Core Strategy](https://gitlab.com/tgdp/governance/-/blob/main/Core_Strategy.md), and in December 2022, we showed our commitment to the open-source ethos by migrating from GitHub to [GitLab](https://gitlab.com/tgdp). We also defined our [template roadmap](https://docs.google.com/document/d/1-39ICPebIoELViysy2Qo5oIS4KrzC_vCb7mvi9RGptE/edit).

In May 2023, The Good Docs Project [joined the GitLab Partner Program](). This is an important milestone for our project, and helps us work toward our goal of setting the standards in technical writing, and becoming a [keystone project](https://thegooddocsproject.dev/blog/becoming-a-keystone-project/).

From our humble beginnings we have grown to over 60 members and created dozens of high-quality, peer-reviewed templates. 2023 will also see the release of the first official version (Version 1.0) of our templates—comprising software and community documentation packs to help you create great docs!

**[Try our templates today](https://gitlab.com/tgdp/templates)!**

## Fast facts

* Conceived in 2019, The Good Docs Project is global, with over 60 active, distributed community members.
* Free and open-source templates, licensed under Zero-Clause BSD.
* Written in markdown, stored in GitLab.
* Templates for core software documentation and open-source communities.
* GitLab Open Source Partners since May, 2023.

---

**Image credits:** Baby doctopus image based on Катя Мамаева's [baby octopus](https://dribbble.com/shots/3609753-Baby-octopus) image. Used with permission from Катя Мамаева.
